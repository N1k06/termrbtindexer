import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class TermRBTMap {
	
	private TreeMap<Long, TermList> tmap;
	
	public TermRBTMap() {
		tmap = new TreeMap<Long, TermList>();
	}
	
	public void putValue(long key, String value) {
		//Checks if the map already contains other events with that timepoint.
		TermList termList = tmap.get(key);
		
		if ( termList == null ){
			//New element initialization
			TermList termListNew = new TermList();
			
			//If there are no other events with the specified timepoint, 
			//the event must be added to the list.
			termListNew.l.add(value);
			termListNew.s = termListNew.l.toString();
			//Add the new element to the map.
			tmap.put(key, termListNew);
		} else {
			//If there are already other events happening in that timepoint,
			//the list must be checked for duplicates.
			if (!termList.l.contains(value)){
				//The corresponding list of events is updated.
				termList.l.add(value);
				termList.s = termList.l.toString();
			}
		}
	}
	
	public void putValue(int key, String value) {
		putValue((long) key,value);
		}
	
	public String getValue(long key) {
		if (tmap.containsKey(key))
			return tmap.get(key).s;
		else
			return "[]";
	}
	
	public String getValue(int key) {
		return getValue((long) key);
	}
	
	public String futureTrace(long key){
		
		Set<Entry<Long, TermList>> set = tmap.tailMap(key+1).entrySet();
		
		StringBuilder sb = new StringBuilder();
        sb.append('[');
		
		if (!set.isEmpty())
		{
			Iterator<Entry<Long, TermList>> i = set.iterator();
	        			
	        for (;;) {
	        	Entry<Long, TermList> e = i.next();
	            sb.append(e.getValue().s);
	            if (! i.hasNext())
	                break;
	            	//return sb.append(']').toString();
	            sb.append(',');
	        }	
		}
		
		return sb.append(']').toString();
	}
	
	public String futureTrace(int key){
		return futureTrace((long) key);
	}
	
	/*
	//Call it after inserting all elements from the trace.
	public void snapshot(){
		Set<Entry<Integer, EventList>> set = tmap.entrySet();
		Iterator<Entry<Integer, EventList>> i = set.iterator();
		
        for (;;) {
            Entry<Integer, EventList> e = i.next();
            e.getValue().s = e.getValue().l.toString();
            if (! i.hasNext())
            	break;
        }
	}
	*/
}
