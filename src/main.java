public class main {

   public static void main(String args[]) {

      /* This is how to declare TreeMap */
      TermRBTMap tmap = 
             new TermRBTMap();
    
      /*Adding elements to TreeMap*/
      tmap.putValue(-1,"prova1");
      tmap.putValue(120,"prova2");
      tmap.putValue(1498052220000L,"prova3");

      System.out.println(tmap.futureTrace(-2L));
      System.out.println(tmap.getValue(1498052220000L));
   }
}